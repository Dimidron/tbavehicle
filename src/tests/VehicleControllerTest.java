package tests;

import static org.junit.Assert.assertThrows;


import org.junit.jupiter.api.Test;

import controller.VehicleController;

class VehicleControllerTest {


	@Test
	void testStopVehicleWrongID() {
		VehicleController controller = new VehicleController();
		int vehicleID = 2;
		assertThrows(NullPointerException.class, () -> controller.stopVehicle(vehicleID));
	}
	@Test
	void testShowCurrentCoordinateWrongID() {
		VehicleController controller = new VehicleController();
		int vehicleID = 2;
		assertThrows(NullPointerException.class, () -> controller.showCurrentCoordinate(vehicleID));
	}


}
