package assist;

public enum CommandEnum {
	HELP("h"), START("start"), END("end"), DELETE("del"), PRINTALL("printall"), SHOW("show"), ADD("add"), REMOVE("remove"), MOVE("move"), SPEED("speed"), STOP("stop");
	String value;

	CommandEnum(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
}
