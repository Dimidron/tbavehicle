package controller;
import java.util.Scanner;

import javax.swing.JFrame;

import assist.CommandEnum;

public class InputController {
	VehicleController controller = null;
	
	public InputController(VehicleController controller) {
		this.controller = controller;
	}
	
	public void startInterection() {

		getHelp();
		JFrame frame = createUI();

		try (Scanner sc = new Scanner(System.in)) {
			while (true) {
				String input = sc.nextLine();
				if(input.equals(CommandEnum.HELP.getValue())) getHelp();
				if(input.equals(CommandEnum.START.getValue())) frame.setVisible(true);
				if(input.equals(CommandEnum.END.getValue())) frame.setVisible(false);
				if(input.contains(CommandEnum.SHOW.getValue())) controller.showSpecificCoordinates(input);
				if(input.equals(CommandEnum.PRINTALL.getValue())) controller.showAllCurrentCoordinates();
				if(input.equals(CommandEnum.ADD.getValue())) controller.addVehicleToSystem();
				if(input.contains(CommandEnum.DELETE.getValue())) controller.deleteVehicleFromSystem(input);
				if(input.contains(CommandEnum.MOVE.getValue())) controller.moveSpecificVehicle(input);
				if(input.contains(CommandEnum.STOP.getValue())) controller.stopSpecificVehicle(input);
				if(input.contains(CommandEnum.SPEED.getValue())) controller.changeSpeed(input);
			}
		}
	}
	
	public void getHelp() {
		System.out.println("-------------------Dies ist ein Manual-------------------");
		System.out.println("h                 ....get this help");
		System.out.println("start             ....start animation");
		System.out.println("end               ....end animation");
		System.out.println("show <ID>         ....show current vehicle coordinate");
		System.out.println("printall          ....show current all vehicle coordinate");
		System.out.println("add               ....create a new Vehicle");
		System.out.println("del <ID>          ....delete a specific Vehicle");
		System.out.println("move <ID> <X> <Y> ....move a specific Vehicle direction -9 to 9");
		System.out.println("stop <ID>         ....stop a specific Vehicle");
		System.out.println("speed <SPEED>     ....change speed of all vehicles speed 1-30 ");
		System.out.println("-------------------Manual ist zu Ende--------------------");
	}
	


	JFrame createUI() {
		JFrame frame = new JFrame();
		frame.add(controller.getPanel());
		frame.setSize(controller.getTerrain().getHeight()+50,controller.getTerrain().getWidth()+50);
		frame.setLocationRelativeTo(null);
		frame.setVisible(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		return frame;
	}
}
