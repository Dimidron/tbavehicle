package controller;

import java.awt.Point;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Terrain;
import model.Vehicle;
import ui.UIPanel;
/**
 * 
 * @author Dmytro Polyak
 *
 * Class for managing the vehicle movements and parsing user inputs.
 * Creating only for this class for presentation purpose.
 * By some conventions and own experience if a code needs comments it's a bad code or a framework
 *
 */
public class VehicleController {
	/**
	 * Variable to generate vehicle IDs. 
	 * They are necessary to control specific vehicles
	 */
	int vehicleID = 0;
	/**
	 * 
	 * Terrain is a specific Terrain where the vehicle will be moved 
	 * This terrain will be used as a border to reflect with
	 * Hardcoded hight and width for presentation purpose
	 */
	Terrain terrain = new Terrain(600, 600);
	/**
	 * Class for visualize the vehicles on
	 */
	UIPanel panel = new UIPanel(terrain);
	
	/**
	 * Getter
	 * @return terrain
	 */
	public Terrain getTerrain() {
		return terrain;
	}
	/**
	 * Setter
	 * @param terrain
	 */
	public void setTerrain(Terrain terrain) {
		this.terrain = terrain;
	}
	/**
	 * Getter 
	 * @return panel
	 */
	public UIPanel getPanel() {
		return panel;
	}
	/**
	 * Setter
	 * @param panel
	 */
	public void setPanel(UIPanel panel) {
		this.panel = panel;
	}
	/**
	 * Method to initialize a vehicle
	 * Calls a method to start a thread
	 */
	public void addVehicleToSystem() {

		int height = 20;
		int width = 20;
		Vehicle vehicle = new Vehicle(height, width);
		vehicle.setVehicleID(vehicleID);
		vehicle.setPanel(panel);
		createVehicle(vehicle);
		terrain.getVehicles().put(vehicleID, vehicle);
		vehicleID++;
		System.out.println("New Vehicle added");
	}
	/**
	 * This method parses an command line input to get an ID from the cmd
	 * Calls a method to remove vehicle from HashMap 
	 * @param stringToMatch
	 */
	public void deleteVehicleFromSystem(String stringToMatch) {
		Pattern pattern = Pattern.compile("del (\\d++)");
		Matcher matcher = pattern.matcher(stringToMatch);

		try {
			if (matcher.find()) {
				int vehicleID = Integer.parseInt(matcher.group(1));
				terrain.getVehicles().get(vehicleID).setVehicleID(-1); //vehicleID -1 form Killing vehicles Thread
				terrain.getVehicles().remove(vehicleID);
				System.out.println("Vehicle "+vehicleID+" succefuly deleted");
			}
		} catch (Exception e) {
			System.err.println("No such Vehicle");
		}
	}
	/**
	 * This method stops the vehicles movement 
	 * the direction coordinates are setting to 0
	 * @param vehicleID
	 */
	public void stopVehicle(int vehicleID) {
		try {
			terrain.getVehicles().get(vehicleID).getDirectionPoint().setLocation(0, 0);
			System.out.println("Vehicle "+vehicleID+" stopped");
		} catch (IndexOutOfBoundsException e) {
			System.err.println("No such Vehicle");
		}
	}
	/**
	 * sets direction of a specific vehicle in the terrain 
	 * @param vehicleID vehicle to move
	 * @param direction x and y coordinates
	 */
	public void moveVehicle(int vehicleID, Point direction) {
		try {
			terrain.getVehicles().get(vehicleID).getDirectionPoint().setLocation(direction);
		} catch (IndexOutOfBoundsException e) {
			System.err.println("No such Vehicle");
		}
	}
	/**
	 * Displays in the command line x and y coordinate of a specific vehicle
	 * @param vehicleID
	 */
	public void showCurrentCoordinate(int vehicleID) {
		try {
			Point snapshotCoordinate = terrain.getVehicles().get(vehicleID).getLocationPoint();
			String coordinateString = "[x="+snapshotCoordinate.x+",y="+snapshotCoordinate.y+"]";
			System.out.println("Snapshot coordinate of Vehicle "+vehicleID+" is "+coordinateString);
		} catch (IndexOutOfBoundsException e) {
			System.err.println("No such Vehicle");
		}
	}
	/**
	 * Displays in the command line x and y coordinate of a all vehicles
	 */
	public void showAllCurrentCoordinates() {
		for (Vehicle vehicle : terrain.getVehicles().values()) {
			Point snapshotCoordinate = vehicle.getLocationPoint();
			String coordinateString = "[x="+snapshotCoordinate.x+",y="+snapshotCoordinate.y+"]";
			System.out.println("Snapshot coordinate of Vehicle "+vehicle.getVehicleID()+" is: "+coordinateString);
		}
	}
	/**
	 * Changing the vehicle movement speed by multiplying the the direction coordinates
	 * 
	 * @param speed
	 */
	public void setSpeed(int speed) {
		if (speed >= 1 && speed <= 30) {
			for (Vehicle vehicle : terrain.getVehicles().values()) {
				vehicle.setSpeed(speed);
			}
		} else
			System.out.println("Please enter speed between 1 and 30");
	}
	/**
	 * Moves the vehicle by changing the current coordinate to the next one
	 * @param vehicle
	 */
	public void moveCurrentCoordinate(Vehicle vehicle) {
		vehicle.getLocationPoint().move(
				vehicle.getLocationPoint().x + vehicle.getDirectionPoint().x * vehicle.getSpeed(),
				vehicle.getLocationPoint().y + vehicle.getDirectionPoint().y * vehicle.getSpeed());
	}
	/**
	 * For presentation purpose calculate the reflection from the Border.
	 * 
	 * @param vehicle
	 * @param terrain
	 */
	public void reflectTerrain(Vehicle vehicle, Terrain terrain) {

		if (vehicle.getLocationPoint().y > terrain.getHeight() - vehicle.getHeight()
				|| vehicle.getLocationPoint().y < terrain.getLocationPoint().y) {
			vehicle.getDirectionPoint().y *= -1;
		}
		if (vehicle.getLocationPoint().x > terrain.getHeight() - vehicle.getHeight()
				|| vehicle.getLocationPoint().x < terrain.getLocationPoint().x) {
			vehicle.getDirectionPoint().x *= -1;
		}
	}
	/**
	 * This method parses an command line input to get an ID and the direction to move a specific vehicle from the cmd
	 * Calls the moveVehicle method with parsed parameters
	 * @param stringToMatch
	 */
	public void moveSpecificVehicle(String stringToMatch) {
		Pattern pattern = Pattern.compile("move (\\d++) (-?\\p{Digit}) (-?\\p{Digit})");
		Matcher matcher = pattern.matcher(stringToMatch);
		if (matcher.find()) {
			int vehicleID = Integer.parseInt(matcher.group(1));
			int xCoordinate = Integer.parseInt(matcher.group(2));
			int yCoordinate = Integer.parseInt(matcher.group(3));
			moveVehicle(vehicleID, new Point(xCoordinate, yCoordinate));
		} else {
			System.out.println("Please check your command");
		}

	}
	/**
	 * This method parses an command line input to get an ID from the cmd to stop a vehicle
	 * calls the stopVehicle method with parsed parameter
	 * @param stringToMatch
	 */
	public void stopSpecificVehicle(String stringToMatch) {
		Pattern pattern = Pattern.compile("stop (\\d++)");
		Matcher matcher = pattern.matcher(stringToMatch);
		if (matcher.find()) {
			int vehicleID = Integer.parseInt(matcher.group(1));
			stopVehicle(vehicleID);
		} else {
			System.out.println("No Vehicle found!");
		}
	}
	/**
	 * This method parses an command line input to get an ID from the cmd to display x and y coordinates
	 * Calls the showCurrentCoordinate methon with parsed coordinate
	 * @param stringToMatch
	 */
	public void showSpecificCoordinates(String stringToMatch) {
		Pattern pattern = Pattern.compile("show (\\d++)");
		Matcher matcher = pattern.matcher(stringToMatch);
		try {

			if (matcher.find()) {
				int vehicleID = Integer.parseInt(matcher.group(1));
				showCurrentCoordinate(vehicleID);
			}
		} catch (Exception e) {
			System.err.println("No Vehicle found!");
		}
	}
	/**
	 * This method parses an command line input to get an ID from the cmd to set speed of all vehicles
	 * Calls the setSpeed method with the parsed parameters
	 * @param stringToMatch
	 */
	public void changeSpeed(String stringToMatch) {
		Pattern pattern = Pattern.compile("speed (\\d++)");
		Matcher matcher = pattern.matcher(stringToMatch);
		if (matcher.find()) {
			int speed = Integer.parseInt(matcher.group(1));
			setSpeed(speed);
		} else {
			System.out.println("No Vehicle found!");
		}
	}
	/**
	 * Starts a Thread with an Vehicle object
	 * Ends if the object is deleted
	 * @param vehicle
	 */
	public void createVehicle(Vehicle vehicle) {
		(new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {
						if (vehicle.getVehicleID() == -1) {
							return;
						} else {
							reflectTerrain(vehicle, terrain);
							moveCurrentCoordinate(vehicle);
							panel.repaint();
							Thread.sleep(100);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		})).start();
	}

}
