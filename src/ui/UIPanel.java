package ui;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;

import javax.swing.JPanel;

import model.Terrain;
import model.Vehicle;

public class UIPanel extends JPanel {

	private static final long serialVersionUID = 8440631864318683544L;
	Terrain terrain;

	public UIPanel(Terrain terrain) {
		this.terrain = terrain;

	}

	@Override
	public void paint(Graphics graphics) {
		super.paint(graphics);

		if (!terrain.getVehicles().isEmpty()) {
			for (Vehicle vehicle : terrain.getVehicles().values()) {

				int xUpperLeftCoordinate = vehicle.getLocationPoint().x;
				int yUpperLeftCoordinate = vehicle.getLocationPoint().y;
				graphics.drawOval(xUpperLeftCoordinate, yUpperLeftCoordinate, vehicle.getHeight(), vehicle.getWidth());

				String vehicleLabel = String.valueOf(vehicle.getVehicleID());
				FontMetrics fontMetrix = graphics.getFontMetrics();
				double textWidth = fontMetrix.getStringBounds(vehicleLabel, graphics).getWidth();

				graphics.setColor(Color.BLACK);

				int xCoordinateOfVehicleLabel = (int) (vehicle.getLocationPoint().x + vehicle.getHeight() / 2
						- textWidth / 2);
				int yCoordinateOfVehicleLabel = (int) (vehicle.getLocationPoint().y + vehicle.getWidth() / 2
						+ fontMetrix.getMaxAscent() / 2);

				graphics.drawString(vehicleLabel, xCoordinateOfVehicleLabel, yCoordinateOfVehicleLabel);
				graphics.drawRect(terrain.getLocationPoint().x, terrain.getLocationPoint().x, terrain.getHeight(),
						terrain.getWidth());
			}
		}

	}

}
