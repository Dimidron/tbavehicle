package model;

import java.util.HashMap;

public class Terrain extends PhysicalObject{
	HashMap<Integer,Vehicle> vehicles = new HashMap<Integer,Vehicle>();

	public Terrain(int height, int width) {
		super(height, width);
	}
	public HashMap<Integer,Vehicle> getVehicles() {
		return vehicles;
	}
	public void setVehicles(HashMap<Integer,Vehicle> vehicles) {
		this.vehicles = vehicles;
	}
	
}
