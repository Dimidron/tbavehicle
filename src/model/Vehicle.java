package model;

import java.awt.Point;

import ui.UIPanel;

public class Vehicle extends PhysicalObject {

	public Vehicle(int height, int width) {
		super(height, width);
	}

	int speed = 10;
	Point directionPoint = new Point(1, 1);
	int vehicleID = 0;

	UIPanel panel;

	public Vehicle() {
	}

	public void setVehicleID(int vehicleID) {
		this.vehicleID = vehicleID;
	}

	public int getVehicleID() {
		return this.vehicleID;
	}

	public Point getDirectionPoint() {
		return directionPoint;
	}

	public void setPanel(UIPanel panel) {
		this.panel = panel;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getSpeed() {
		return speed;
	}

}
