package model;

import java.awt.Point;

public abstract class PhysicalObject {
	Point locationPoint = new Point(0,0);
	
	int height = 0;
	int width = 0;
	
	public PhysicalObject() {
	}
	
	public PhysicalObject(Point locationPoint) {
		this.locationPoint=locationPoint;
	}
	
	public PhysicalObject(int height, int width) {
		this.height=height;
		this.width=width;
	}
	
	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public Point getLocationPoint() {
		return locationPoint;
	}
	

}
